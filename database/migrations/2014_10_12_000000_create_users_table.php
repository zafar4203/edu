<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('web_email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone')->nullable();
            $table->string('cnic')->nullable();
            $table->string('password');
            $table->text('address')->nullable();
            $table->string('image')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->integer('role_id')->nullable();
            $table->integer('team_id')->nullable();
            $table->integer('isSuper')->default(0);
            $table->integer('status')->default(0);

            $table->bigInteger('basic')->nullable();
            $table->bigInteger('bonus')->nullable();

            $table->string('account_name')->nullable();
            $table->bigInteger('account_number')->nullable();
            $table->bigInteger('branch_code')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
