



@extends('layouts.login')
@section('content')

<div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-4">
			<div class="container-fluid">
				<div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
					<div class="col mx-auto">
						<div class="card mt-5 mt-lg-0">
							<div class="card-body">
								<div class="p-4 rounded">
									<div class="text-center">
										<h3 class="">Sign in</h3>
										<p>Don't have an account yet? <a href="{{ route('register') }}">Sign up here</a>
										</p>
									</div>
					
										<hr/>
									<div class="form-body">
                                        <form method="POST" class="class="row g-3"" action="{{ route('login') }}">
                                        @csrf
                                        <div class="col-12">
												<label for="inputEmailAddress" class="form-label">Email Address</label>
                                                <input id="email" type="email" id="inputEmailAddress" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" required autocomplete="off" autofocus>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                            </div>
											<div class="col-12 mt-4">
												<label for="inputChoosePassword" class="form-label">Enter Password</label>
												<div class="input-group" id="show_hide_password">
                                                    <input id="inputChoosePassword" type="password" class="form-control  border-end-0 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                    <a href="javascript:;" class="input-group-text bg-transparent" placeholder="Enter Password"><i class='bx bx-hide'></i></a>
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror

												</div>
											</div>
                                            <div class="row mt-3 mb-3">
											<div class="col-md-6">
												<div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" name="remember"  id="flexSwitchCheckChecked" checked {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="flexSwitchCheckChecked">{{ __('Remember Me') }}</label>
												</div>
											</div>
                                            @if (Route::has('password.request'))
                                            <div class="col-md-6 text-end">	    
                                                <a href="{{ route('password.request') }}">
                                                    {{ __('Forgot Password?') }}
                                                </a>
                                            </div>
                                            @endif
                                            </div>
											<div class="col-12">
												<div class="d-grid">
													<button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Sign in</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end row-->
			</div>
		</div>
@endsection
