@extends('layouts.main')
@section('styles')
@endsection
@section('content')
<div class="page-content">
				<div class="row row-cols-1 row-cols-lg-4">
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-cosmic">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">MCQS All Time</p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
									<div class="ms-auto text-white">
									<p class="mb-0 text-white">Last 30</p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-burning">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Events All Time</p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
									<div class="ms-auto text-white">
									<p class="mb-0 text-white">Last 30 </p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-Ohhappiness">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Emails All Time</p>
										<h5 class="mb-0 text-white">1,212</h5>
									</div>
									<div class="ms-auto text-white">
									<p class="mb-0 text-white">Last 30 Days</p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-gradient-moonlit">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Chats All Time</p>
										<h5 class="mb-0 text-white">2,869</h5>
									</div>
									<div class="ms-auto text-white">
									<p class="mb-0 text-white">Last 30 Days</p>
										<h5 class="mb-0 text-white">1000</h5>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div><!--end row-->
				
				
				
				<div class="row row-cols-1 row-cols-lg-3">
				
				
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Mcqs</p>
										<h5 class="font-weight-bold">Today: 30,842 <small class="text-success font-13">(+40%)</small></h5>
										<h6 class="font-weight-bold">Yesterday: 32,842</h6>
										<p class="text-success mb-0 font-13">Analytics for last 2 Days</p>
									</div>
									<div class="widgets-icons bg-gradient-cosmic text-white"><i class='bx bx-refresh'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Events</p>
										<h5 class="font-weight-bold">Today: 30,842 <small class="text-success font-13">(+40%)</small></h5>
										<h6 class="font-weight-bold">Yesterday: 32,842</h6>
										<p class="text-success mb-0 font-13">Analytics for last 2 Days</p>
									</div>
									<div class="widgets-icons bg-gradient-burning text-white"><i class='bx bx-group'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Blog</p>
										<h5 class="font-weight-bold">This Month: 30,842 <small class="text-success font-13">(+40%)</small></h5>
										<h6 class="font-weight-bold">Last Month: 32,842</h6>
										<p class="text-success mb-0 font-13">Analytics for last 2 Months</p>
									</div>
									<div class="widgets-icons bg-gradient-lush text-white"><i class='bx bx-note'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Emails</p>
										<h5 class="font-weight-bold">Today: 23 <small class="text-danger font-13">(-10%)</small></h5>
										<h6 class="font-weight-bold">Yesterday: 20</h6>
										<p class="text-danger mb-0 font-13">Analytics for last 2 Days</p>
									</div>
									<div class="widgets-icons bg-gradient-kyoto text-white"><i class='bx bxs-envelope'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Chats</p>
										<h5 class="font-weight-bold">Today: 30,842 <small class="text-success font-13">(+40%)</small></h5>
										<h6 class="font-weight-bold">Yesterday: 32,842</h6>
										<p class="text-success mb-0 font-13">Analytics for last 2 Days</p>
									</div>
									<div class="widgets-icons bg-gradient-blues text-white"><i class='bx bx-chat'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										<p class="mb-0">Admins Salary</p>
										<h5 class="font-weight-bold">This Month: 30,842 <small class="text-success font-13">(+40%)</small></h5>
										<h6 class="font-weight-bold">Last Month: 32,842</h6>
										<p class="text-success mb-0 font-13">Analytics for last 2 Months</p>
									</div>
									<div class="widgets-icons bg-gradient-moonlit text-white"><i class='bx bx-bar-chart'></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<!--end row-->
                  <!--start-->
				<div class="row row-cols-1 row-cols-lg-3">
				
                   <div class="col d-flex">
					<div class="card radius-10 w-100">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<h6 class="font-weight-bold mb-0">Admins</h6>
								</div>
								<div class="dropdown ms-auto">
									<div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded"></i>
									</div>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="javaScript:;">More</a>
									</div>
								</div>
							</div>
						   </div>
							<div class="best-selling-products p-3 mb-3">
								
								<div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-success">Mcqs</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-danger">Blog</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-warning">Events</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-danger font-18">(+54%)</small> <span class="badge badge-pill bg-info">Course</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								     <hr/>  
									 
									 <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-success">Mcqs</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-danger">Blog</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-success font-18">(+54%)</small> <span class="badge badge-pill bg-warning">Events</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								       <hr/>
									   
									   <div class="d-flex align-items-center">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
									</div>
									<div class="ps-3">
										<h6 class="mb-0 font-weight-bold">Admin Name <small class="text-danger font-18">(+54%)</small> <span class="badge badge-pill bg-info">Course</span></h6>
										<p class="mb-0 font-18 text-secondary">
									 
									<small class="text-gray font-15"><strong>(T: 324 / Y: 3,233 / M: 60,000)</strong></small>
									<small class="text-info font-15"><strong>(Comp: 2)</strong></small>
									<small class="text-warning font-15"><strong>(Pend: 3)</strong></small>
									
									</p>
									</div>
									</div>
								     <hr/> 
								
								
							</div>
					</div>
				   </div>
				   
				   
				   <div class="col d-flex">
					<div class="card radius-10 w-100">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<h6 class="font-weight-bold mb-0">Emails</h6>
								</div>
								<div class="dropdown ms-auto">
									<div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded"></i>
									</div>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="javaScript:;">More</a>
									</div>
								</div>
							</div>
						  </div>
							<div class="recent-reviews p-3 mb-3">
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							<div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
								<div class="">
									<img src="assets/images/avatars/avatar-1.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="ms-2">
									<h6 class="mb-1 font-14">Subject: ABC</h6>
									<p class="mb-0 font-18 text-secondary">
									<span class="badge badge-pill bg-info">Send: Admin</span> 
									<span class="badge badge-pill bg-danger">Recieved: Admin</span>  &nbsp;</p>
								</div>
								<div class="">
									<img src="assets/images/avatars/avatar-2.png" class="rounded-circle" width="46" height="46" alt="" />
								</div>
								<div class="list-inline d-flex customers-contacts ms-auto">	<a href="#" class="list-inline-item"><i class='bx bxs-envelope'></i></a>
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
							</div>
							
							
							</div>
						
					</div>
				   </div>
				   
				   
				   <div class="col d-flex">
					<div class="card radius-10 w-100">
						<div class="card-body">
							<div class="d-flex align-items-center">
							
								<div>
									<h6 class="font-weight-bold mb-0">Chat</h6>
								</div>
								<div class="dropdown ms-auto">
									<div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded"></i>
									</div>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="javaScript:;">More</a>
									</div>
								</div>
							</div>
						  </div>
							<div class="support-list p-3 mb-3">
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-1.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Jordan Ntolo <span class="text-primary float-end font-13">2 hours ago</span></h6>
										<p class="mb-0 font-13 text-secondary">My item doesn't ship to correct address. Please check It Proper</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-2.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Carolien Bloeme <span class="text-primary float-end font-13">3 hours ago</span></h6>
										<p class="mb-0 font-13 text-secondary">You shipped different color, I need it to be changed</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-3.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Lisanne Viscall <span class="text-primary float-end font-13">12 hours ago</span></h6>
										<p class="mb-0 font-13 text-secondary">Can you please refund my money. I don't want to wait anymore</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-4.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Sun Jun <span class="text-primary float-end font-13">12 Yesterday</span></h6>
										<p class="mb-0 font-13 text-secondary">Please return my phone. it is not iPhon7. I send you many request.</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-5.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Lotila Remo <span class="text-primary float-end font-13">2 days ago</span></h6>
										<p class="mb-0 font-13 text-secondary">Hello, I need admin template product. how can i purchase?</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-2.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Carolien Bloeme <span class="text-primary float-end font-13">3 hours ago</span></h6>
										<p class="mb-0 font-13 text-secondary">You shipped different color, I need it to be changed</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-3.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Lisanne Viscall <span class="text-primary float-end font-13">12 hours ago</span></h6>
										<p class="mb-0 font-13 text-secondary">Can you please refund my money. I don't want to wait anymore</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								<hr/>
								<div class="d-flex align-items-top">
									<div class="">
										<img src="assets/images/avatars/avatar-4.png" width="45" height="45" class="rounded-circle" alt="" />
									</div>
									<div class="ps-2">
										<h6 class="mb-1 font-weight-bold">Sun Jun <span class="text-primary float-end font-13">12 Yesterday</span></h6>
										<p class="mb-0 font-13 text-secondary">Please return my phone. it is not iPhon7. I send you many request.</p>
									</div>
									<div class="list-inline d-flex customers-contacts ms-auto">
									<a href="#" class="list-inline-item"><i class='bx bxs-chat' ></i></a>
								</div>
								</div>
								
								
							</div>
						
					</div>
				   </div>
				   
				</div>
				<!--end row-->
				
				

				<div class="card radius-10">
					<div class="card-header border-bottom-0 bg-transparent">
						<div class="d-flex align-items-center">
							<div>
								<h5 class="font-weight-bold mb-0">Recent Added</h5>
							</div>
							<div class="ms-auto">
								<button type="button" class="btn btn-white radius-10">View More</button>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table mb-0 align-middle">
								<thead>
									<tr>
										<th>Photo</th>
										<th>Title</th>
										<th>Admin Name</th>
										<th>Location</th>
										<th>Admin Team</th>
										<th>Role</th>
										<th>Category</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">MCQS</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">Event</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">MCQS</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">MCQS</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">Blog</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<div class="product-img bg-transparent border">
												<img src="assets/images/icons/smartphone.png" class="p-1">
											</div>
										</td>
										<td><div class="ms-2">
														<h6 class="mb-1 font-14"><a href="javaScript:" target="_blank">Light Red T-Shirt Light Red T-Shirt Light Red T-Shirt </a></h6>
													</div></td>
										<td>Zafer Khan</td>
										<td>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Pakistan</a>
										<a href="javaScript:;" class="btn btn-sm btn-info radius-30">Punjab</a>
										<a href="javaScript:;" class="btn btn-sm btn-primary radius-30">Rawalpindi</a>
										</td>
										<td>Yellow</td>
										<td>Yellow Lead</td>
										<td> 
										     <a href="javaScript:;" class="btn btn-sm btn-info radius-30">MCQS</a>
											 <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">Math</a> 
										     <a href="javaScript:;" class="btn btn-sm btn-success radius-30">SCC</a>
											 <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">FIA</a>
											 </td>
										<td>
										<div class="d-flex order-actions">	<a href="javascript:;" class="text-danger bg-light-danger border-0"><i class='bx bxs-trash'></i></a>
													<a href="javascript:;" class="ms-4 text-primary bg-light-primary border-0"><i class='bx bxs-edit' ></i></a>
												</div>
										</td>
									</tr>
									
								</tbody>
							</table>
							
						</div>
					</div>
				</div>
			</div>
@endsection
@section('scripts')
@endsection