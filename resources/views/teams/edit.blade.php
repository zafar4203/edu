@extends('layouts.main')
@section('content')
<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Dashboard</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Edit Team</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('teams.index') }}" type="button" class="btn btn-primary">Back</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<div class="row row-cols-1 row-cols-2">


					<div class="col-12">
						<h6 class="mb-0 text-uppercase">Edit Team Form</h6>
						<hr>
						<div class="card border-top border-0 border-4 border-primary">
							<div class="card-body p-5">
								<form method="post" action="{{ route('teams.update') }}"  enctype="multipart/form-data" class="row g-3">
                                 @csrf
                                 <input type="hidden" name="id" value="{{ $team->id }}">
									<div class="col-md-6">
										<label for="inputFirstName" class="form-label">Name</label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" id="inputFirstName" name="name" value="{{ $team->name }}">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="inputseo_title" class="form-label">Seo Title</label>
										<input type="text" class="form-control @error('seo_title') is-invalid @enderror" id="inputseo_title" name="seo_title" value="{{ $team->seo_title }}">
                                        @error('seo_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-12">
										<label for="inputseo_description" class="form-label">Seo Description</label>
										<textarea class="form-control @error('seo_description') is-invalid @enderror" id="inputseo_description" placeholder="Seo description..." rows="3" name="seo_description">{{ $team->seo_description }}</textarea>
                                        @error('seo_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-md-6">
										<label for="inputFile" class="form-label">Profile Image (optional)</label>
                                        <input name="file" class="form-control" type="file" id="inputFile">
                                        @error('file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									<div class="col-md-12 mt-5">
										<button type="submit" class="btn btn-primary px-5">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
				<!--end row-->	
			</div>
			
@endsection