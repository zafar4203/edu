<div class="sidebar-wrapper" data-simplebar="true">
			<div class="sidebar-header">
				<div>
					<img src="{{ asset('public/assets/images/logo-icon.png') }}" class="logo-icon" alt="logo icon">
				</div>
				<div>
					<h4 class="logo-text">Synadmin</h4>
				</div>
				<div class="toggle-icon ms-auto"><i class='bx bx-first-page'></i>
				</div>
			</div>
			<!--navigation-->
			<ul class="metismenu" id="menu">
				<li>
					<a href="javascript:;" class="has-arrow">
						<div class="parent-icon"><i class='bx bx-home'></i>
						</div>
						<div class="menu-title">Dashboard</div>
					</a>
					<ul>
						<li> <a href="index.html"><i class="bx bx-right-arrow-alt"></i>Dashboard</a>
						</li>
						<li> <a href="index2.html"><i class="bx bx-right-arrow-alt"></i>MCQS Posts</a>
						</li>
						<li> <a href="index3.html"><i class="bx bx-right-arrow-alt"></i>Events Posts</a>
						</li>
						<li> <a href="index4.html"><i class="bx bx-right-arrow-alt"></i>Blog Posts</a>
						</li>
						<li> <a href="index4.html"><i class="bx bx-right-arrow-alt"></i>Admins</a>
						</li>
						
					</ul>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-envelope' ></i>
						</div>
						<div class="menu-title">Email</div>
					</a>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-chat' ></i>
						</div>
						<div class="menu-title">Chat Box</div>
					</a>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-task' ></i>
						</div>
						<div class="menu-title">Tasks</div>
					</a>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-calendar' ></i>
						</div>
						<div class="menu-title">Calendar</div>
					</a>
				</li>
				
				
				<li>
					<a class="has-arrow" href="javascript:;">
						<div class="parent-icon"><i class='bx bx-hourglass' ></i>
						</div>
						<div class="menu-title">Data</div>
					</a>
					<ul>
						<li> <a href="form-elements.html"><i class="bx bx-right-arrow-alt"></i>Add MCQS</a>
						</li>
						<li> <a href="form-input-group.html"><i class="bx bx-right-arrow-alt"></i>Add Event</a>
						</li>
						<li> <a href="form-layouts.html"><i class="bx bx-right-arrow-alt"></i>Add Blog</a>
						</li>
						<li> <a href="{{ route('admins.index') }}"><i class="bx bx-right-arrow-alt"></i>Admins</a>
						</li>
						<li> <a href="{{ route('teams.index') }}"><i class="bx bx-right-arrow-alt"></i>Teams</a>
						</li>
						<li> <a href="{{ route('roles.index') }}"><i class="bx bx-right-arrow-alt"></i>Roles</a>
						</li>
						<li> <a href="{{ route('categories.index' , 'all') }}"><i class="bx bx-right-arrow-alt"></i>Categories</a>
						</li>
						<li> <a href="{{ route('tags.index' , 'all') }}"><i class="bx bx-right-arrow-alt"></i>Tags</a>
						</li>
						<li> <a href="form-wizard.html"><i class="bx bx-right-arrow-alt"></i>etc</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-grid-alt' ></i>
						</div>
						<div class="menu-title">Export Data</div>
					</a>
				</li>
				
				<li>
					<a href="user-profile.html">
						<div class="parent-icon"><i class='bx bx-user-pin' ></i>
						</div>
						<div class="menu-title">User Profile</div>
					</a>
				</li>
				
				
				<li>
					<a href="faq.html">
						<div class="parent-icon"><i class="bx bx-help-circle"></i>
						</div>
						<div class="menu-title">FAQ</div>
					</a>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-lock' ></i>
						</div>
						<div class="menu-title">Lock Screen</div>
					</a>
				</li>
				
				<li>
					<a href="#0">
						<div class="parent-icon"><i class='bx bx-log-out-circle' ></i>
						</div>
						<div class="menu-title">Logout</div>
					</a>
				</li>
				
			</ul>
			<!--end navigation-->
			
			
			
		</div>