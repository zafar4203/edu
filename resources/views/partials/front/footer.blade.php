<section class="footer-area footer-02 bg_cover" style="background-image: url(assets/images/counter-bg.jpg);">
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-link mt-45">
                            <h4 class="footer-title">Information</h4>
                            <ul class="link-list">
                                <li><a href="#">Admission</a></li>
                                <li><a href="#">Tuition fee</a></li>
                                <li><a href="#">Scholorship</a></li>
                                <li><a href="#">Coditions</a></li>
                                <li><a href="#">Facilities</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-link mt-45">
                            <h4 class="footer-title">Useful Link</h4>
                            <ul class="link-list">
                                <li><a href="our-courses-left-sidebar.html">All Courses</a></li>
                                <li><a href="teachers.html">Our Teachers</a></li>
                                <li><a href="event.html">Our Events</a></li>
                                <li><a href="blog-left-sidebar.html">Blog post</a></li>
                                <li><a href="faq.html">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-link mt-45">
                            <h4 class="footer-title">Useful Link</h4>
                            <ul class="link-list">
                                <li><a href="#">All Courses</a></li>
                                <li><a href="#">Our Teachers</a></li>
                                <li><a href="#">Our Events</a></li>
                                <li><a href="#">Blog post</a></li>
                                <li><a href="login.html">Login</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-link mt-45">
                            <h4 class="footer-title">Contact Info</h4>
                            <ul class="link-list">
                                <li>
                                    <p>245, New Town, Marklen Street North City, New York, USA</p>
                                </li>
                                <li>
                                    <p><a href="tel:+01254659874">+01254 659 874 </a></p>
                                    <p><a href="tel:+32145857458">+32145 857 458</a></p>
                                </li>
                                <li>
                                    <p><a href="mailto://info@example.com">info@example.com</a></p>
                                    <p></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-widget-wrapper">
                    <div class="footer-social">
                        <ul class="social">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer-menu">
                        <ul class="menu">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Courses</a></li>
                            <li><a href="#">Notice Board</a></li>
                            <li><a href="#">Offers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="copyright text-center">
                    <p>&copy; Copyright all right reserved by <a href="https://hasthemes.com/">Hasthemes</a></p>
                </div>
            </div>
        </div>
    </section>