@extends('layouts.main')
@section('styles')
    <link href="{{ asset('public/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection
@section('content')
<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Dashboard</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Add Blog</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('blogs.index') }}" type="button" class="btn btn-primary">Back</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<div class="row row-cols-1 row-cols-2">


					<div class="col-12">
						<h6 class="mb-0 text-uppercase">Add Blog Form</h6>
						<hr>
						<div class="card border-top border-0 border-4 border-primary">
							<div class="card-body p-5">
								<form method="post" action="{{ route('blogs.store') }}"  enctype="multipart/form-data" class="row g-3">
                                 @csrf           
                                 <div class="col-md-12">
										<label for="inputtitle" class="form-label">Title</label>
										<input type="text" class="form-control @error('title') is-invalid @enderror" id="inputtitle" name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-12">
										<label for="inputdetails" class="form-label">Details</label>
										<textarea class="form-control @error('text') is-invalid @enderror" id="inputdetails" placeholder="Details about Blog" rows="3" name="text">{{ old('text') }}</textarea>
                                        @error('text')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>                                   

                                    <div class="col-md-6">
										<label for="inputcity" class="form-label">City</label>
										<input type="text" class="form-control @error('city') is-invalid @enderror" id="inputcity" name="city" value="{{ old('city') }}">
                                        @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputCountry" class="form-label">Country</label>
										<input type="text" class="form-control @error('country') is-invalid @enderror" id="inputCountry" name="country" value="{{ old('country') }}">
                                        @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputstate" class="form-label">State</label>
										<input type="text" class="form-control @error('state') is-invalid @enderror" id="inputstate" name="state" value="{{ old('state') }}">
                                        @error('state')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
			
                                    <div class="col-md-6">
										<label for="inputType" class="form-label">Category</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->name}}</option>
											@endforeach
                                        </select>
                                        @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    
                                    <div class="col-md-6">
										<label for="inputType" class="form-label">Tag</label>
                                        <select class="form-control" name="tag_id" id="tag_id">
                                            <option value="0">None</option>
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('tag_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    
                                    <div class="col-md-6">
                                        <label class="form-label">Child Tags</label>
    										<select disabled name="childtags_id[]" class="multiple-select" data-placeholder="Choose anything" id="child_tags" multiple="multiple">
										</select>
                                    </div>

									<div class="col-md-6">
										<label for="inputFile" class="form-label">Profile Image (optional)</label>
                                        <input name="file" class="form-control" type="file" id="inputFile">
                                        @error('file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-12">
										<label for="inputseo_title" class="form-label">Seo Title</label>
										<input type="text" class="form-control @error('seo_title') is-invalid @enderror" id="inputseo_title" name="seo_title" value="{{ old('seo_title') }}">
                                        @error('seo_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									
                                    <div class="col-12">
										<label for="inputseo_description" class="form-label">Seo Description</label>
										<textarea class="form-control @error('seo_description') is-invalid @enderror" id="inputseo_description" placeholder="Seo description..." rows="3" name="seo_description">{{ old('seo_description') }}</textarea>
                                        @error('seo_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>                                   


									<div class="col-md-12 mt-5">
										<button type="submit" class="btn btn-primary px-5">Add Blog</button>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
				<!--end row-->	
			</div>
			
@endsection
@section('scripts')

<script src="{{ asset('public/assets/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $('.multiple-select').select2({
        theme: 'bootstrap4',
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });

    $('#tag_id').change(function(){
        var id = this.value;
        var url = 'http://localhost/edu/blog/allChildTags';
        $.ajax({
            type:'GET',
            url:url+'/'+id,
            success:function(data){
                if(data.tags.length > 0){
                    $('#child_tags').removeAttr('disabled');
                    var html = '';
                    data.tags.forEach(function(tag){
                        html += '<option value="'+tag.id+'">'+tag.name+'</option>'
                    });
                    $('#child_tags').html(html);
                    console.log(data.tags);
                }
            }
        });
    });
</script>

@endsection