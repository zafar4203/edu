<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    
    <!--====== Title ======-->
    <title>Edumate Education HTML Template</title>
    
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('public/front/assets/images/favicon.png') }}" type="image/png">
    
        
    <!-- CSS
    ============================================ -->

    <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->

    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/default.css') }}">


    <!--===== Plugins CSS (All Plugins Files) =====-->
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/plugins/magnific-popup.css') }}">

    <!--====== Main Style CSS ======-->
    <link rel="stylesheet" href="{{ asset('public/front/assets/css/style.css') }}">
    @yield('styles')
</head>

<body>
    
   @include('partials.front.header')
    
    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        </section>

    <!--====== Page Banner Ends ======-->

   @yield('content')
    
    <!--====== Newsletter Start ======-->

    <section class="newsletter-area-2">
        <div class="container">
            <div class="newsletter-wrapper bg_cover" style="background-image: url(assets/images/newsletter-bg-1.jpg);">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="section-title-2 mt-25">
                            <h2 class="title">Subscribe our Newsletter</h2>
                            <span class="line"></span>
                            <p>Even slightly believable. If you are going use a passage of Lorem Ipsum need some</p>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="newsletter-form mt-30">
                            <form action="#">
                                <input type="text" placeholder="Enter your email here">
                                <button class="main-btn main-btn-2">Subscribe now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Newsletter Ends ======-->
    
    @include('partials.front.footer')
    <!--====== BACK TOP TOP PART START ======-->

    <a href="#" class="back-to-top"><i class="fal fa-chevron-up"></i></a>

    <!--====== BACK TOP TOP PART ENDS ======-->
    
    <!--====== Start ======-->

<!--
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-">
                    
                </div>
            </div>
        </div>
    </section>
-->

    <!--====== Ends ======-->




    <!--====== Jquery js ======-->
    <script src="{{ asset('public/front/assets/js/vendor/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
    
    <!--====== All Plugins js ======-->
    <script src="{{ asset('public/front/assets/js/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/slick.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/wow.min.js') }}"></script>
    <script src="{{ asset('public/front/assets/js/plugins/ajax-contact.js') }}"></script>
    

    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

    <!-- <script src="assets/js/plugin.min.js"></script> -->

    
    <!--====== Main Activation  js ======-->
    <script src="{{ asset('public/front/assets/js/main.js') }}"></script>
    
    @yield('scripts')
</body>

</html>
