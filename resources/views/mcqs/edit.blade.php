@extends('layouts.main')
@section('styles')
    <link href="{{ asset('public/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection
@section('content')
<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Dashboard</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Add Mcq</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('mcqs.index') }}" type="button" class="btn btn-primary">Back</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<div class="row row-cols-1 row-cols-2">


					<div class="col-12">
						<h6 class="mb-0 text-uppercase">Edit MCQ Form</h6>
						<hr>
						<div class="card border-top border-0 border-4 border-primary">
							<div class="card-body p-5">
								<form method="post" action="{{ route('mcqs.update') }}"  enctype="multipart/form-data" class="row g-3">
                                 @csrf           
                                <input type="hidden" name="id" value="{{ $mcq->id }}">                                 
                                 <div class="col-md-12">
										<label for="inputquestion" class="form-label">Question</label>
										<input type="text" class="form-control @error('question') is-invalid @enderror" id="inputquestion" name="question" value="{{ $mcq->question }}">
                                        @error('question')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-12">
										<label for="inputoptions" class="form-label">Options</label>
										<textarea class="form-control @error('options') is-invalid @enderror" id="inputoptions" placeholder="Options with commas" rows="3" name="options">{{ $mcq->options }}</textarea>
                                        @error('options')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>                                   

                                    <div class="col-md-6">
										<label for="inputc_answer" class="form-label">Correct Answers</label>
										<input type="text" class="form-control @error('c_answer') is-invalid @enderror" id="inputc_answer" name="c_answer" value="{{ $mcq->c_answer }}">
                                        @error('c_answer')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									
                                    <div class="col-md-6">
										<label for="inputType" class="form-label">Category</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($categories as $category)
												<option {{$mcq->category_id == $category->id ? 'selected' : ''}} value="{{ $category->id }}">{{ $category->name}}</option>
											@endforeach
                                        </select>
                                        @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    
                                    <div class="col-md-6">
										<label for="inputType" class="form-label">Tag</label>
                                        <select class="form-control" name="tag_id" id="tag_id">
                                            <option value="0">None</option>
                                            @foreach($tags as $tag)
                                                <option {{$mcq->tag_id == $tag->id ? 'selected' : ''}} value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('tag_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    
                                    <div class="col-md-6">
                                        <label class="form-label">Child Tags</label>
    										<select {{ !isset($mcq->childtags_id) ? 'disabled' : ''}} name="childtags_id[]" class="multiple-select" data-placeholder="Choose anything" id="child_tags" multiple="multiple">
                                            @if($mcq->childtags_id)
                                            @foreach(json_decode($mcq->childtags_id) as $ctag)
                                            <option selected value="{{ $ctag }}">{{ \App\ChildTag::childName($ctag)}}</option>             
                                            @endforeach  
                                            @endif                                 
										</select>
                                    </div>


                                    <div class="col-md-6">
										<label for="inputcity" class="form-label">City</label>
										<input type="text" class="form-control @error('city') is-invalid @enderror" id="inputcity" name="city" value="{{ $mcq->city }}">
                                        @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputCountry" class="form-label">Country</label>
										<input type="text" class="form-control @error('country') is-invalid @enderror" id="inputCountry" name="country" value="{{ $mcq->country }}">
                                        @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputstate" class="form-label">State</label>
										<input type="text" class="form-control @error('state') is-invalid @enderror" id="inputstate" name="state" value="{{ $mcq->state }}">
                                        @error('state')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									<div class="col-md-6">
										<label for="inputFile" class="form-label">Profile Image (optional)</label>
                                        <input name="file" class="form-control" type="file" id="inputFile">
                                        @error('file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-12">
										<label for="inputseo_title" class="form-label">Seo Title</label>
										<input type="text" class="form-control @error('seo_title') is-invalid @enderror" id="inputseo_title" name="seo_title" value="{{ $mcq->seo_title }}">
                                        @error('seo_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									
                                    <div class="col-12">
										<label for="inputseo_description" class="form-label">Seo Description</label>
										<textarea class="form-control @error('seo_description') is-invalid @enderror" id="inputseo_description" placeholder="Seo description..." rows="3" name="seo_description">{{ $mcq->seo_description }}</textarea>
                                        @error('seo_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>                                   


									<div class="col-md-12 mt-5">
										<button type="submit" class="btn btn-primary px-5">Save Mcq</button>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
				<!--end row-->	
			</div>
			
@endsection
@section('scripts')

<script src="{{ asset('public/assets/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $('.multiple-select').select2({
        theme: 'bootstrap4',
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });

    $('#tag_id').change(function(){
        var id = this.value;
        var url = 'http://localhost/edu/mcq/allChildTags';
        $.ajax({
            type:'GET',
            url:url+'/'+id,
            success:function(data){
                if(data.tags.length > 0){
                    $('#child_tags').removeAttr('disabled');
                    var html = '';
                    data.tags.forEach(function(tag){
                        html += '<option value="'+tag.id+'">'+tag.name+'</option>'
                    });
                    $('#child_tags').html(html);
                    console.log(data.tags);
                }
            }
        });
    });
</script>

@endsection