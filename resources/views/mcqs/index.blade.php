@extends('layouts.main')
@section('content')

<div class="page-content">
			
        <!-- row-->
        <div class="fm-search">
            <div class="mb-0">
                <div class="input-group input-group-lg">	<span class="input-group-text bg-transparent"><i class="bx bx-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search Admin">
                </div>
            </div>
        </div>
        <hr>
        <div class="card radius-10 mt-3">
            <div class="card-header border-bottom-0 bg-transparent">
                <div class="d-flex align-items-center">
                    <div>
                        <h5 class="font-weight-bold mb-0">Mcqs</h5>
                    </div>
                    <div class="ms-auto">
                <div class="btn-group">
                    <a href="{{ route('mcqs.add') }}" type="button" class="btn btn-info">Add New MCQ</a>
                    <button type="button" class="btn btn-primary">Filters</button>
                    <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">	<span class="visually-hidden">Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">
                        <a class="dropdown-item" href="javascript:;">All</a>
                        <a class="dropdown-item" href="javascript:;">Date Descending</a>
                        <a class="dropdown-item" href="javascript:;">Date Ascending</a>
                        <a class="dropdown-item" href="javascript:;">Team Yellow</a>
                        <a class="dropdown-item" href="javascript:;">Team Red</a>
                        <a class="dropdown-item" href="javascript:;">Team Green</a>
                        
                    </div>
                </div>
            </div>
                </div>
            </div>
            <div class="card-body">
            @include('partials.flash')
                <div class="table-responsive">
                    <table class="table mb-0 align-middle">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Question</th>
                                <th>Admin</th>
                                <th>location</th>
                                <th>Admin Team</th>
                                <th>Role</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mcqs as $mcq)
                            <tr>
                                <td>
                                    <a href="{{ route('mcqs.edit' , $mcq->id) }}">
                                        <div class="product-img bg-transparent border">
                                            <img src="{{ $mcq->image ? $mcq->image : asset('public/assets/images/avatars/avatar-1.png') }}" class="rounded-circle" width="46" height="46" title="Admin Name">
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('mcqs.edit' , $mcq->id) }}">
                                            <div class="ms-2">
                                                <h6 class="mb-1 font-14"><a href="#" target="_blank">{{ $mcq->question }}</a></h6>
                                            </div>
                                    </a>
                                </td>
                                <td>{{ $mcq->user->name }}</td>
                                <td>
                                    @if(isset($mcq->country))
                                    <a href="javaScript:;" class="btn btn-sm btn-primary radius-30">{{ $mcq->country }}</a>
                                    @endif
                                    @if(isset($mcq->state))
                                    <a href="javaScript:;" class="btn btn-sm btn-info radius-30">{{ $mcq->state }}</a>
                                    @endif
                                    @if(isset($mcq->city))
                                    <a href="javaScript:;" class="btn btn-sm btn-primary radius-30">{{ $mcq->city }}</a>
                                    @endif
                                </td>
                                @if(isset($mcq->user->team))
                                <td>{{ $mcq->user->team->name }}</td>
                                @endif
                                <td>{{ $mcq->user->role != 0 ? $mcq->user->role->name : 'Super Admin' }}</td>
                                <td> 
                                    <!-- category -->
                                    @if(isset($mcq->category))
                                    <a href="javaScript:;" class="btn btn-sm btn-info radius-30">{{ $mcq->category->name }}</a>
                                    @endif
                                    <!-- tag -->
                                    @if(isset($mcq->tag))
                                    <a href="javaScript:;" class="btn btn-sm btn-warning radius-30">{{ $mcq->tag->name }}</a> 
                                    @endif
                                    <!-- child tags -->
                                    @php 
                                    $childTags = json_decode($mcq->childtags_id);
                                    if(count($childTags) > 0){
                                    @endphp
                                        @foreach($childTags as $ct)
                                        <a href="javaScript:;" class="btn btn-sm btn-danger radius-30">{{ App\ChildTag::childName($ct) }}</a>
                                        @endforeach
                                    @php 
                                    }
                                    @endphp
                                </td>
                                <td>
                                    @if($mcq->status == 0)
                                        <div class="badge rounded-pill text-success bg-light-success p-2 text-uppercase px-3"><i class="bx bxs-circle me-1"></i>Active</div>
                                    @else
                                        <div class="badge rounded-pill text-danger bg-light-success p-2 text-uppercase px-3"><i class="bx bxs-circle me-1"></i>In Active</div>
                                    @endif
                                </td>
                                <td>
                                    <div class="d-flex order-actions">	
                                        <a href="{{ route('mcqs.edit' , $mcq->id) }}" class="ms-1 text-primary bg-light-primary border-0"><i class="bx bxs-edit"></i></a>
                                        <a data-href="{{ route('mcqs.delete' , $mcq->id) }}" class="text-danger bg-light-danger border-0 delete"><i class="bx bxs-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <div class="col-md-12 mt-5  text-center">
                        {{ $mcqs->withQueryString()->links() }}
                    </div>

                   
                </div>
            </div>
        </div>
        
        <!-- row end-->
    </div>

@endsection
@section('scripts')
<script>
    $('.delete').click(function(){
        var link = $(this).data('href');    
		swal({
              title: 'Are you sure?',
              text: 'Once deleted, you will not be able to recover this Review!',
              icon: 'warning',
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
					location.href = link;      
				}else {
				swal('Your User is safe');
				}
		});
    }); 
</script>
@endsection


