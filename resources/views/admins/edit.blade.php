@extends('layouts.main')
@section('content')
<div class="page-content">
				
				<div class="container">
					<div class="main-body">
						<div class="row">
							<div class="col-lg-4">
								<div class="card">
									<div class="card-body">
										<div class="d-flex flex-column align-items-center text-center">
											<img src="{{ $user->image ? $user->image : asset('public/assets/images/avatars/avatar-2.png') }}" alt="Admin" class="rounded-circle p-1 bg-primary" width="110">
											<div class="mt-3">
												<h4>Qasim Ali</h4>
												<p class="text-secondary mb-1">MCQS Admin</p>
												<p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
												<button class="btn btn-outline-primary">Email</button>
												<button class="btn btn-outline-primary">Message</button>
											</div>
										</div>
										<hr class="my-4">
										<ul class="list-group list-group-flush">
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0"> Basic</h6>
												<span class="text-secondary">{{ $user->basic }} </span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0"> No Leave</h6>
												<span class="text-secondary">1000</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0"> Task on Time</h6>
												<span class="text-secondary">1000</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Quality Entries</h6>
												<span class="text-secondary">1000</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Event Bonous</h6>
												<span class="text-secondary">1000 </span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Employee of the Month</h6>
												<span class="text-secondary">1000 </span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Other</h6>
												<span class="text-secondary">- -</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Total</h6>
												<span class="text-secondary">15000 </span>
											</li>
										</ul>
										
										<hr class="my-4">
										<ul class="list-group list-group-flush">
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">Admin of</h6>
												<span class="text-secondary">MCQS</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">City</h6>
												<span class="text-secondary">{{ $user->city }}</span>
											</li>
											<li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
												<h6 class="mb-0">State</h6>
												<span class="text-secondary">{{ $user->state }}</span>
											</li>											
										</ul>										
										
									</div>
								</div>
							</div>
							<div class="col-lg-8">
                            <form action="{{ route('admins.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}">
								<div class="card">
									<div class="card-body">
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Name</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}">
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Username</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control @error('name') is-invalid @enderror" name="username" value="{{ $user->username }}">
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Email</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Web Email</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control @error('web_email') is-invalid @enderror" name="web_email" value="{{ $user->web_email}}">
                                                @error('web_email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Phone</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $user->phone}}">
                                                @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Address</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{$user->address}}">
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">CNIC</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control @error('cnic') is-invalid @enderror" name="cnic" value="{{$user->cnic}}">
                                                @error('cnic')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Team</h6>
											</div>
											<div class="col-sm-9 text-secondary">
                                                <select name="team_id" class="form-select  @error('team_id') is-invalid @enderror" id="inputTeam" required="">
                                                    <option selected="" disabled="" value="">Choose...</option>
                                                    <option {{ $user->team_id == 1 ? 'selected' : '' }} value="1">Team 1</option>
                                                    <option>...</option>
                                                </select>
                                                @error('team_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
                                        <div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Role</h6>
											</div>
											<div class="col-sm-9 text-secondary">
                                                <select name="role_id" class="form-select  @error('role_id') is-invalid @enderror" id="inputRoleId" required="">
                                                    <option selected="" disabled="" value="">Choose...</option>
                                                    <option {{ $user->role_id == 1 ? 'selected' : '' }} value="1">Role 1</option>
                                                    <option>...</option>
                                                </select>
                                                @error('role_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
											</div>
										</div>
										<hr>
										
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Account Name</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control" value="Alflah">
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Account #</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control" value="ADFG23232223232323">
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-sm-3">
												<h6 class="mb-0">Branch Code</h6>
											</div>
											<div class="col-sm-9 text-secondary">
												<input type="text" class="form-control" value="0323">
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3"></div>
											<div class="col-sm-9 text-secondary">
												<input type="submit" class="btn btn-primary px-4" value="Save Changes">
											</div>
										</div>
									</div>
								</div>
                            </form>
								<div class="row">
									<div class="col-sm-12">
										<div class="card">
											<div class="card-body">
												<h5 class="d-flex align-items-center mb-3">Project Status</h5>
												<p>All Time Entries (13000)</p>
												<div class="progress mb-3" style="height: 5px">
													<div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<p>This Month (3000)</p>
												<div class="progress mb-3" style="height: 5px">
													<div class="progress-bar bg-danger" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<p>Last 2232</p>
												<div class="progress mb-3" style="height: 5px">
													<div class="progress-bar bg-success" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            @endsection