@extends('layouts.main')
@section('content')
<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Dashboard</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Add User</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('admins.index') }}" type="button" class="btn btn-primary">Back</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<div class="row row-cols-1 row-cols-2">


					<div class="col-12">
						<h6 class="mb-0 text-uppercase">Add User Form</h6>
						<hr>
						<div class="card border-top border-0 border-4 border-primary">
							<div class="card-body p-5">
								<form method="post" action="{{ route('admins.store') }}"  enctype="multipart/form-data" class="row g-3">
                                 @csrf
									<div class="col-md-6">
										<label for="inputFirstName" class="form-label">Name</label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" id="inputFirstName" name="name" value="{{ old('name') }}">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="inputusername" class="form-label">User Name</label>
										<input type="text" class="form-control @error('username') is-invalid @enderror" id="inputusername" name="username" value="{{ old('username') }}">
                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-md-6">
										<label for="inputEmail" class="form-label">Email</label>
										<input type="email" class="form-control @error('email') is-invalid @enderror" id="inputEmail" name="email" value="{{ old('email') }}">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="inputWebEmail" class="form-label">Web Email</label>
										<input type="email" class="form-control @error('web_email') is-invalid @enderror" id="inputWebEmail" name="web_email" value="{{ old('web_email') }}">
                                        @error('web_email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="inputPhone" class="form-label">Phone</label>
										<input type="number" class="form-control @error('phone') is-invalid @enderror" id="inputPhone" name="phone" value="{{ old('phone') }}">
                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="inputCNIC" class="form-label">CNIC</label>
										<input type="number" class="form-control @error('cnic') is-invalid @enderror" id="inputCNIC" name="cnic" value="{{ old('cnic') }}">
                                        @error('cnic')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-md-6">
										<label for="inputPassword" class="form-label">Password</label>
										<input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="inputPassword">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
                                    <div class="col-md-6">
										<label for="confirmPassword" class="form-label">Confirm Password</label>
										<input type="password" name="cn_password" class="form-control" id="confirmPassword">
									</div>
									<div class="col-12">
										<label for="inputAddress" class="form-label">Address</label>
										<textarea class="form-control @error('address') is-invalid @enderror" id="inputAddress" placeholder="Address..." rows="3" name="address">{{ old('address') }}</textarea>
                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="col-md-6">
										<label for="inputCity" class="form-label">City</label>
										<input type="text" class="form-control @error('city') is-invalid @enderror" id="inputCity" name="city" value="{{ old('city') }}">
									    @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="col-md-6">
										<label for="inputState" class="form-label">State</label>
										<input type="text" class="form-control @error('state') is-invalid @enderror" id="inputState" name="state" value="{{ old('state') }}">
									    @error('state')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

									<div class="col-md-6">
										<label for="inputTeam" class="form-label">Team</label>
                                        <select name="team_id" class="form-select  @error('team_id') is-invalid @enderror" id="inputTeam" required="">
                                            <option selected="" disabled="" value="">Choose...</option>
                                            <option {{ old('team_id') == 1 ? 'selected' : '' }} value="1">Team 1</option>
                                            <option>...</option>
                                        </select>
                                        @error('team_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputRole" class="form-label">Role</label>
                                        <select name="role_id" class="form-select @error('role_id') is-invalid @enderror" id="inputRole" required="">
                                            <option selected="" disabled="" value="">Choose...</option>
                                            <option {{ old('role_id') == 1 ? 'selected' : '' }} value="1">Role 1</option>
                                            <option>...</option>
                                        </select>
                                        @error('role_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

                                    <div class="col-md-6">
										<label for="inputBasic" class="form-label">Basic</label>
										<input type="number" class="form-control @error('basic') is-invalid @enderror" id="inputBasic" name="basic" value="{{ old('basic') }}">
									    @error('basic')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-6">
										<label for="inputBonus" class="form-label">Bonus</label>
										<input type="number" class="form-control @error('bonus') is-invalid @enderror" id="inputBonus" name="bonus" value="{{ old('bonus') }}">
									    @error('bonus')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

									<div class="col-md-6">
										<label for="inputFile" class="form-label">Profile Image (optional)</label>
                                        <input name="file" class="form-control" type="file" id="inputFile">
                                        @error('file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									<div class="col-md-12 mt-5">
										<button type="submit" class="btn btn-primary px-5">Add User</button>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
				<!--end row-->	
			</div>
			
@endsection