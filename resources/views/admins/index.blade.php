@extends('layouts.main')
@section('content')

<div class="page-content">
			
        <!-- row-->
        <div class="fm-search">
            <div class="mb-0">
                <div class="input-group input-group-lg">	<span class="input-group-text bg-transparent"><i class="bx bx-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search Admin">
                </div>
            </div>
        </div>
        <hr>
        <div class="card radius-10">
            <div class="card-header border-bottom-0 bg-transparent">
                <div class="d-flex align-items-center">
                    <div>
                        <h5 class="font-weight-bold mb-0">User Data</h5>
                    </div>
                    <div class="ms-auto">
                <div class="btn-group">
                    <a href="{{ route('admins.add') }}" type="button" class="btn btn-info">Add New User</a>
                    <button type="button" class="btn btn-primary">Filters</button>
                    <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">	<span class="visually-hidden">Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">
                        <a class="dropdown-item" href="javascript:;">All</a>
                        <a class="dropdown-item" href="javascript:;">Date Descending</a>
                        <a class="dropdown-item" href="javascript:;">Date Ascending</a>
                        <a class="dropdown-item" href="javascript:;">Team Yellow</a>
                        <a class="dropdown-item" href="javascript:;">Team Red</a>
                        <a class="dropdown-item" href="javascript:;">Team Green</a>
                        
                    </div>
                </div>
            </div>
                </div>
            </div>
            <div class="card-body">
            @include('partials.flash')
                <div class="table-responsive">
                    <table class="table mb-0 align-middle">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Location</th>
                                <th>Role</th>
                                <th>Team</th>
                                <th>Total Entries</th>
                                <th>This Month</th>
                                <th>Last Month</th>
                                <th>Today</th>
                                <th>Basic</th>
                                <th>Bonus</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                <a href="{{ route('admins.edit' , $user->id) }}">
                                    <div class="product-img bg-transparent border">
                                        <img src="{{ $user->image ? $user->image : asset('public/assets/images/avatars/avatar-1.png') }}" class="rounded-circle" width="46" height="46" title="Admin Name">
                                    </div>
                                </a>
                                </td>
                                <td>
                                <a href="{{ route('admins.edit' , $user->id) }}">
                                        <div class="ms-2">
                                            <h6 class="mb-1 font-14"><a href="#" target="_blank">{{ $user->name }}</a></h6>
                                        </div>
                                    </a>
                                </td>
                                <td> 
                                    <a href="javaScript:;" class="btn btn-sm btn-info radius-30">MCQS</a>
                                </td>
                                <td>{{$user->city}} / {{ $user->state}}</td>
                                <td>Admin</td>
                                <td>Yello</td>
                                <td>23,333</td>
                                <td>2,121</td>
                                <td>3250</td>
                                <td>346</td>
                                <td>{{ $user->basic }}</td>
                                <td>{{ $user->bonus }}</td>
                                <td>
                                    <div class="d-flex order-actions">	
                                        <a href="{{ route('admins.edit' , $user->id) }}" class="ms-1 text-primary bg-light-primary border-0"><i class="bx bxs-edit"></i></a>
                                        <a data-href="{{ route('admins.delete' , $user->id) }}" class="text-danger bg-light-danger border-0 delete"><i class="bx bxs-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <div class="col-md-12 mt-5  text-center">
                        {{ $users->withQueryString()->links() }}
                    </div>

                   
                </div>
            </div>
        </div>
        
        <!-- row end-->
    </div>

@endsection
@section('scripts')
<script>
    $('.delete').click(function(){
        var link = $(this).data('href');    
		swal({
              title: 'Are you sure?',
              text: 'Once deleted, you will not be able to recover this Review!',
              icon: 'warning',
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
					location.href = link;      
				}else {
				swal('Your User is safe');
				}
		});
    }); 
</script>
@endsection


