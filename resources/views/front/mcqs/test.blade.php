@extends('layouts.front')
@section('styles')
<style>
    .hide{
        display:none;
    }
    .unclickable{
        pointer-events: none;
    }
</style>
@endsection
@section('content')    
<section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{ asset('public/front/assets/images/page-banner.jpg') }});">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">IQ Test</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="faq-area">
        <div class="container">
		
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-2">
                        <h2 class="title">How Genius You Are ?</h2>
                        <span class="line"></span>
                        <p>Here is the Quick Way to Learn, Just Click on Option and You will see the Correct Answer Under Each Question.</p>                    
					</div>
                </div>
            </div>
			
			<div class="row">
                <div class="col-lg-12">
                    <div class="newsletter-form mt-30">
                            <form action="#">
                                <input type="text" placeholder="Enter Your Name">
                                <button class="main-btn main-btn-2">Start Test</button>
								
                            </form>
                        </div>
                </div>	
            </div>			
			
			<div class="courses-bar">
                <div class="row">
                    <div class="col-lg-3">
                        
                    </div>
                    <div class="col-lg-12">
                        <div class="courses-bar-form">
						
						<div class="courses-select">
                                <select id="selectbox1">
                                    <option value="0">Choose Country</option>
                                    <option value="1">Pakistan</option>
									<option value="1">India</option>
									<option value="1">Etc</option>
                                    
                                </select>
                            </div> 
							
						    <div class="courses-select">
                                <select id="selectbox1">
                                    <option value="0">Choose Subject</option>
									<option value="1">G.Knoweledge</option>
									<option value="1">Math</option>
									<option value="1">Accounting</option>
                                    
                                </select>
                            </div>
							
							<div class="courses-select">
                                <select id="selectbox1">
                                    <option value="0">Choose Level</option>
									<option value="1">High</option>
									<option value="2">Modriate</option>
                                    <option value="3">Easy</option>
                                </select>
                            </div>
							
                            <div class="courses-select">
                                <select id="selectbox1">
                                    <option value="0">Question Limit</option>
                                    <option value="1">25</option>
                                    <option value="2">50</option>
                                    <option value="3">100</option>
                                </select>
                            </div>

                            <span class="float-right"><i class="far fa-clock" style="color:red"></i> <span class="Timer"></span></span>

                        </div>
                    </div>
                </div>            
            </div>    
            <div class="faq-wrapper">
                <div class="accordion" id="accordionExample">
                    
					<!-- S -->
                    @foreach($mcqs as $mcq)
					<div class="card">
                        <span class="question">Q:</span>
                        <div class="card-header" id="heading1">
                            <a href="#" data-toggle="collapse" data-target="#collapse1"> {{ $mcq->question }} </a><br><br>
							<a href="#" data-toggle="collapse" data-target="#collapse1">
                            <ul class="slider-btn">
                                @php 
                                    $options =  explode(',', $mcq->options);                                
                                    $alpha = ['A','B','C','D','E'];
                                @endphp
                                @foreach($options as $key => $option)
                                    <li data-correct="{{$mcq->c_answer}}" id="option_{{$mcq->id}}_{{$alpha[$key]}}" data-value="{{$option}}" data-id="{{$mcq->id}}" data-key="{{$alpha[$key]}}" data-animation="fadeInUp" data-delay="0.6s" class="main-btn main-btn-3 option">{{$alpha[$key]}}: {{ $option }}</li><br>
                                @endforeach
                                <li data-animation="fadeInUp" data-delay="0.6s" class="hide correct_answer main-btn main-btn-2 mt-3">D: {{ $mcq->c_answer}}</li>

                            </ul>
                            </a>
						</div>                  
                    </div>
                    @endforeach
					<!-- S -->					
                </div>
            </div>
			
			<div class="pagination-items text-center">
				<button id="show_result" class="main-btn main-btn-2">Show Result</button>
            </div>
				
        </div>
    </section>
	
	<!--====== Result Board Starts ======-->
	<div id="r_board" class="hide counter-area-2">
        <div class="container">
		                    <span class="float-right"><i class="far fa-calendar" style="color:red"></i> 24 Dec, 2022 <i class="far fa-clock" style="color:red"></i> 12:32 Pm</span>
		<h2 class="title">Result Board : Educatehow.com</h2> <hr />
            <div class="counter-wrapper-2 bg_cover" style="background-image: url(assets/images/counter-bg-2.jpg);">
			
			
                <div class="row">
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.2s">
                            <span class="counter-count">Hammas Ahmed</span>
                            <p>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.4s">
                            <span class="counter-count">Mixed</span>
                            <p>Subject</p>
                        </div>
                    </div>
					<div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.4s">
                            <span class="counter-count">All</span>
                            <p>Country</p>
                        </div>
                    </div>
					<div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.6s">
                            <span class="counter-count">Easy</span>
                            <p>Test Level</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.6s">
                            <span class="counter-count"><span class="count">54</span> / 90</span>
                            <p>Completed Time (mins)</p>
                        </div>
                    </div>
                    
					<div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.6s">
                            <span class="counter-count"><span id="wrong_count" class="count">10</span> / {{count($mcqs)}}</span>
                            <p>Wrong Answers</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.6s">
                            <span class="counter-count"><span id="correct_count" class="count">90</span> / {{count($mcqs)}}</span>
                            <p>Correct Answers</p>
                        </div>
                    </div>
					
                </div>
				
				<div class="single-counter mt-30 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.8s">
                    <p>Mr. / Mrs. (Hammas Ahmed) Passed This Test/Exam in ( <i class="far fa-clock" style="color:green"></i> <span id="timer"> </span>  ) Time.</p>
                </div>
            </div>
								
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        var start = new Date;
        var timer = null,
        interval = 1000,
        value = 0;

        if (timer !== null) return;
        timer = setInterval(function() {
            $('.Timer').text((new Date - start) / 1000 + " Seconds");
        }, interval);
        

        var questions_limit = {{ count($mcqs) }};
        var answers = [];
        var count = 0;
        var p_count = 0;
        var n_count = 0;
        
        $('.option').click(function(){
            var id = this.id;
            var mcq_id = $(this).data('id');
            var value = $(this).data('value');
            var key = $(this).data('key');
            var correct = $(this).data('correct');
            count++;
            let obj = {id:mcq_id , c_answer:correct , selected: value}
            answers.push(obj);
            $('#'+id).removeClass('main-btn-3').addClass('clicked');
            $('#option_'+mcq_id+'_A').addClass('unclickable');
            $('#option_'+mcq_id+'_B').addClass('unclickable');
            $('#option_'+mcq_id+'_C').addClass('unclickable');
            $('#option_'+mcq_id+'_D').addClass('unclickable');
            $('#option_'+mcq_id+'_E').addClass('unclickable');

        });

        $('#show_result').click(function(){
            if(count == questions_limit){
                clearInterval(timer);
                timer = null
                answers.forEach(function(value){
                    if(value.c_answer == value.selected){
                        p_count++;
                    }else{
                        n_count++;
                    }
                });
                $('.correct_answer').removeClass('hide');                
                $('#wrong_count').html(n_count);                
                $('#correct_count').html(p_count);     
                $('#timer').html($('.Timer').html());                                
                $('#r_board').removeClass('hide');                
            }else{
                alert("Please Choose all answers");
            }
        });
    });
</script>
@endsection