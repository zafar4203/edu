@extends('layouts.front')
 <!--====== Top Course Start ======-->
@section('content')    
 <section class="top-courses-area">
        <div class="container">
		<h2 class="title">Our Courses</h2>
            <div class="courses-bar">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="courses-tab">
                            <ul class="nav" role="tablist">
                                <li><a data-toggle="tab" class="active" href="#grid" role="tab"><i class="fas fa-th-large"></i></a></li>
                                <li><a data-toggle="tab" href="#list" role="tab"><i class="fas fa-list"></i></a></li>
                            </ul>
                            <p>Showing 1 - 16 of 36 results</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="courses-bar-form">
                            <div class="courses-select">
                                <select id="selectbox1">
                                    <option value="0">By Country</option>
                                    <option value="1">Pakistan</option>
                                    <option value="1">India</option>
                                    <option value="1">Canada</option>
                                </select>
                            </div>
                            <div class="courses-search">
                                <input type="text" placeholder="Search Test or Exam">
                                <i class="far fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>    
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="grid" role="tabpanel">
                    <div class="courses-wrapper wrapper-2">
                        <div class="row">
                            
                            @foreach($tags as $tag)
							<div class="col-lg-3 col-sm-6 courses-col">
                                <div class="single-courses-2 mt-30">
                                    <div class="courses-image">
                                        <a href="{{ route('mcq.test' , [$category->id , $tag->id]) }}"><img src="{{ asset('public/front/assets/images/courses/courses-6.jpg') }}" alt="courses"></a>
                                    </div>
                                    <div class="courses-content">
                                        <a href="#" class="category">#Subject</a>
                                        <h4 class="courses-title"><a href="{{ route('mcq.test' , [$category->id , $tag->id]) }}">{{ $tag->name }}</a></h4>
                                        <div class="duration-rating">
                                            <div class="duration-fee">
                                                <p class="duration">MCQS </p>
                                                <p class="fee">Country </p>
                                            </div>
											<div class="rating">
                                                <span>Rating: </span>
                                                <ul class="star">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="courses-link">
                                            <a class="apply" href="#0">Start Prepration</a>
                                            <a class="more" href="#0">Start Test <i class="fal fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
              
            </div>        
            

            <ul class="pagination-items text-center">
                <li><a class="active" href="#">01</a></li>
                <li><a href="#">02</a></li>
                <li><a href="#">03</a></li>
                <li><a href="#">04</a></li>
                <li><a href="#">05</a></li>
            </ul>
        </div>
    </section>

    <!--====== Top Course Ends ======-->
@endsection