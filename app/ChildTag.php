<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildTag extends Model
{
    protected $guarded = ['id'];

    public function tag(){
        return $this->belongsTo('App\Tag','tag_id');
    }

    public function parent(){
        return $this->belongsTo('App\ChildTag','parent_id');
    }

    public function allChildTags()
    {
        return $this->hasMany('App\ChildTag', 'id', 'parent_id');
    }

    public static function childName($id)
    {
        $tag = ChildTag::where(['id' => $id])->first();
        return $tag->name;
    }

}
