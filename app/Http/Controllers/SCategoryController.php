<?php

namespace App\Http\Controllers;

use Validator;
use App\Category;
use Illuminate\Http\Request;

class SCategoryController extends Controller
{
    public function index($type = null){
        if($type && $type != 'all'){
            $categories = Category::where(['type' => $type])->paginate(10);
        }else{
            $categories = Category::paginate(10);
        }
        return view('categories.index', compact('categories' , 'type'));
    }

    public function add(){
        return view('categories.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name',
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'type' => 'nullable|max:255',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $category = new Category();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $category->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/categories/'.$fileName);
        }
        $request->merge([
            'image' => $img
        ]);
        $category->create($request->all());

        return redirect(route('categories.index'))->with('success','Category Created successfully!');

    }

    public function edit($id){
        $category = Category::find($id);
        return view('categories.edit' , compact('category'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name,'.$request->id,            
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'type' => 'nullable|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $category = Category::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $category->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $category->image = asset('public/assets/images/categories/'.$fileName);
        }

        $category->update($request->all());

        return redirect(route('categories.index'))->with('success','Category Updated successfully!');

    }

    public function delete($id){
        $category = Category::find($id);
        $category->delete();

        return redirect(route('categories.index'))->with('success','Category Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $categories = Category::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($categories);
        if($count > 0)
            return view('categories.index', compact('categories','q'));
        else 
            return redirect(route('categories.index'))->with('error','No Records Found!');  
    }

}

