<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SAdminController extends Controller
{
    public function index(){
        $users = User::where(['status' => 0])->paginate(10);
        return view('admins.index', compact('users'));
    }

    public function add(){
        return view('admins.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'username' => 'required|unique:users,username',
            'phone' => 'required|max:15',
            'cnic' => 'required|max:13|min:13',
            'email' => 'required|email|unique:users,email',
            'web_email' => 'nullable|email',
            'address' => 'nullable|max:500',
            'city' => 'nullable|max:50',
            'state' => 'nullable|max:50',
            'team_id' => 'required',
            'role_id' => 'required',
            'password' => 'required|min:6|required_with:cn_password|same:cn_password',
            'cn_password' => 'min:6',
            'basic' => 'nullable|max:8',
            'bonus' => 'nullable|max:8',
            'account_name' => 'nullable|max:30',
            'account_number' => 'nullable|max:30',
            'branch_code' => 'nullable|max:30',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $user = new User();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/users';
            $fileName = $user->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/users/'.$fileName);
        }
        $request->merge([
            'password' => Hash::make($request->password),
            'image' => $img
        ]);
        $user->create($request->all());

        return redirect(route('admins.index'))->with('success','User Created successfully!');

    }

    public function edit($id){
        $user = User::find($id);
        return view('admins.edit' , compact('user'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'username' => 'required|unique:users,username,'.$request->id,
            'phone' => 'required|max:15',
            'cnic' => 'required|max:13|min:13',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'web_email' => 'nullable|email',
            'address' => 'nullable|max:500',
            'city' => 'nullable|max:50',
            'state' => 'nullable|max:50',
            'team_id' => 'required',
            'role_id' => 'required',
            'basic' => 'nullable|max:8',
            'bonus' => 'nullable|max:8',
            'account_name' => 'nullable|max:30',
            'account_number' => 'nullable|max:30',
            'branch_code' => 'nullable|max:30',

            
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/users';
            $fileName = $user->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $user->image = asset('public/assets/images/users/'.$fileName);
        }

        $user->update($request->all());

        return redirect(route('admins.index'))->with('success','Profile Updated successfully!');

    }

    public function delete($id){
        $user = User::find($id);
        $user->delete();

        return redirect(route('admins.index'))->with('success','User Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $users = User::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($users);
        if($count > 0)
            return view('admins.index', compact('users','q'));
        else 
            return redirect(route('admins.index'))->with('error','No Records Found!');  
    }

    public function assign_task($id){
        $tasks = Task::where(['user_id' => $id])->paginate(10);
        $user = User::find($id);
        return view('admins.assignTask', compact('tasks' , 'user'));
    }

    public function storeTask(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $task = new Task();
        $task->create($request->all());
        return redirect(route('admins.assignTask' , $request->user_id))->with('success','Task Added Successfully!');

    }

    public function delete_task($id , $user_id){
        $task = Task::find($id);
        $task->delete();

        return redirect(route('admins.assignTask' , $user_id))->with('success','Task Deleted Successfully!');
    }

    public function fastUpdate(Request $request){
        $task = Task::find($request->id);
        $task->status = $request->status;
        $task->save();

        return response()->json(array('success' => true , 'msg' =>'Task Updated Successfully'), 200);
    }   

    public function myTasks($id){
        $tasks = Task::where(['user_id' => $id])->paginate(10);
        return view('admins.myTask', compact('tasks'));           
    }
}

