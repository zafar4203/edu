<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Tag;
use App\ChildTag;
use App\Blog;
use App\Category;
use Illuminate\Http\Request;

class SBlogController extends Controller
{
    public function index(){
        $blogs = Blog::paginate(10);
        return view('blogs.index', compact('blogs'));
    }

    public function add(){
        $categories = Category::where(['status' => 0 ,'type' => 'blog'])->get();
        $tags = Tag::where(['status' => 0,'type' => 'blog'])->get();
        return view('blogs.add',compact('categories','tags'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'city' => 'required|max:255',
            'state' => 'required',
            'country' => 'required',
            'title' => 'required',
            'text' => 'required',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $blog = new Blog();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/blogs';
            $fileName = $blog->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/blogs/'.$fileName);
        }
        $request->merge([
            'image' => $img,
            'user_id' => Auth::user()->id,
            'childtags_id' => json_encode($request->childtags_id)
        ]);
        $blog->create($request->all());

        return redirect(route('blogs.index'))->with('success','Blog Created successfully!');

    }

    public function edit($id){
        $blog = Blog::find($id);
        $tags = Tag::where(['status' => 0,'type' => 'blog'])->get();
        $categories = Category::where(['status' => 0,'type' => 'blog'])->get();
        return view('blogs.edit' , compact('tags','categories','blog'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'city' => 'required|max:255',
            'state' => 'required',
            'country' => 'required',
            'title' => 'required',
            'text' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $blog = Blog::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/blogs';
            $fileName = $blog->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $blog->image = asset('public/assets/images/blogs/'.$fileName);
        }

        $blog->update($request->all());

        return redirect(route('blogs.index'))->with('success','Blog Updated successfully!');

    }

    public function delete($id){
        $blog = Blog::find($id);
        $blog->delete();
        return redirect(route('blogs.index'))->with('success','Blog Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $blogs = Blog::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($blogs);
        if($count > 0)
            return view('blogs.index', compact('blogs','q'));
        else 
            return redirect(route('blogs.index'))->with('error','No Records Found!');  
    }

    public function allChildTags($id){
        $tags = ChildTag::where(['tag_id' => $id])
        ->with('allChildTags')
        ->get();

        return response()->json(array('success' => true , 'tags' =>$tags), 200);
    }

}

