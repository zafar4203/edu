<?php

namespace App\Http\Controllers;

use Validator;
use App\Tag;
use App\Category;
use Illuminate\Http\Request;

class STagController extends Controller
{
    public function index($type = null){
        if($type && $type != 'all'){
            $tags = Tag::where(['type' => $type])->paginate(10);
        }else{
            $tags = Tag::paginate(10);
        }
        return view('tags.index', compact('tags','type'));
    }

    public function add(){
        $categories = Category::where(['status' => 0,'type'=>'mcq'])->get();
        return view('tags.add',compact('categories'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name',
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'type' => 'nullable|max:255',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $tag = new Tag();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $tag->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/categories/'.$fileName);
        }
        $request->merge([
            'image' => $img
        ]);
        $tag->create($request->all());

        return redirect(route('tags.index'))->with('success','Tag Created successfully!');

    }

    public function edit($id){
        $tag = Tag::find($id);
        $categories = Category::where(['status' => 0,'type'=>'mcq'])->get();
        return view('tags.edit' , compact('tag','categories'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name,'.$request->id,            
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'type' => 'nullable|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $tag = Tag::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $tag->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $tag->image = asset('public/assets/images/categories/'.$fileName);
        }

        $tag->update($request->all());

        return redirect(route('tags.index'))->with('success','Tag Updated successfully!');

    }

    public function delete($id){
        $tag = Tag::find($id);
        $tag->delete();

        return redirect(route('tags.index'))->with('success','Tag Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $tags = Tag::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($tags);
        if($count > 0)
            return view('tags.index', compact('tags','q'));
        else 
            return redirect(route('tags.index'))->with('error','No Records Found!');  
    }

}

