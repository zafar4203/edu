<?php

namespace App\Http\Controllers;

use Validator;
use App\Role;
use Illuminate\Http\Request;

class SRoleController extends Controller
{
    public function index(){
        $roles = Role::where(['status' => 0])->paginate(10);
        return view('roles.index', compact('roles'));
    }

    public function add(){
        return view('roles.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $role = new Role();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/roles';
            $fileName = $role->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/roles/'.$fileName);
        }
        $request->merge([
            'image' => $img
        ]);
        $role->create($request->all());

        return redirect(route('roles.index'))->with('success','Role Created successfully!');

    }

    public function edit($id){
        $role = Role::find($id);
        return view('roles.edit' , compact('role'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name,'.$request->id,            
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $role = Role::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/roles';
            $fileName = $role->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $role->image = asset('public/assets/images/roles/'.$fileName);
        }

        $role->update($request->all());

        return redirect(route('roles.index'))->with('success','Role Updated successfully!');

    }

    public function delete($id){
        $role = Role::find($id);
        $role->delete();

        return redirect(route('roles.index'))->with('success','Role Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $roles = Role::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($roles);
        if($count > 0)
            return view('roles.index', compact('roles','q'));
        else 
            return redirect(route('roles.index'))->with('error','No Records Found!');  
    }

}

