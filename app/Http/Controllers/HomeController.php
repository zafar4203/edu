<?php

namespace App\Http\Controllers;

use App\Mcq;
use App\Tag;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function categories_mcq(){
        $categories = Category::where(['status' => 0 , 'type' => 'mcq'])->get();
        return view('front/mcqs/category', compact('categories'));
    }

    public function categories_tags($id){
        $category = Category::where(['status' => 0 , 'id' => $id])->first();
        $tags = Tag::where(['status'=> 0 , 'category_id' => $id])->get();
        return view('front/mcqs/tags', compact('tags','category'));
    }

    public function categories_test($cat_id , $tag_id = null , $child_tags = null){
        $mcqs = Mcq::where(['status'=> 0 , 'category_id' => $cat_id , 'tag_id' => $tag_id])->get();
        return view('front/mcqs/test', compact('mcqs'));
    }
}
