<?php

namespace App\Http\Controllers;

use Validator;
use App\Tag;
use App\ChildTag;
use App\Category;
use Illuminate\Http\Request;

class SChildTagController extends Controller
{
    public function index($type = null){
        $tags = ChildTag::paginate(10);
        return view('childtags.index', compact('tags'));
    }

    public function add(){
        $tags = ChildTag::all();
        $ptags = Tag::all();
        return view('childtags.add',compact('tags','ptags'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name',
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'parent_id' => 'required',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $tag = new ChildTag();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $tag->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/categories/'.$fileName);
        }
        $request->merge([
            'image' => $img
        ]);
        $tag->create($request->all());

        return redirect(route('childtags.index'))->with('success','Tag Created successfully!');

    }

    public function edit($id){
        $tag = ChildTag::find($id);
        $tags = ChildTag::where('id','<>',$id)->get();
        $ptags = Tag::all();
        return view('childtags.edit' , compact('tag','tags','ptags'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name,'.$request->id,            
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'parent_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $tag = ChildTag::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/categories';
            $fileName = $tag->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $tag->image = asset('public/assets/images/categories/'.$fileName);
        }

        $tag->update($request->all());

        return redirect(route('childtags.index'))->with('success','Tag Updated successfully!');

    }

    public function delete($id){
        $tag = ChildTag::find($id);
        $tag->delete();

        return redirect(route('childtags.index'))->with('success','Tag Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $tags = ChildTag::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($tags);
        if($count > 0)
            return view('childtags.index', compact('tags','q'));
        else 
            return redirect(route('childtags.index'))->with('error','No Records Found!');  
    }

}

