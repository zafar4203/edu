<?php

namespace App\Http\Controllers;

use Validator;
use App\Team;
use Illuminate\Http\Request;

class STeamController extends Controller
{
    public function index(){
        $teams = Team::where(['status' => 0])->paginate(10);
        return view('teams.index', compact('teams'));
    }

    public function add(){
        return view('teams.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $team = new Team();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/teams';
            $fileName = $team->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/teams/'.$fileName);
        }
        $request->merge([
            'image' => $img
        ]);
        $team->create($request->all());

        return redirect(route('teams.index'))->with('success','Team Created successfully!');

    }

    public function edit($id){
        $team = Team::find($id);
        return view('teams.edit' , compact('team'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u|unique:teams,name,'.$request->id,            
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $team = Team::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/teams';
            $fileName = $team->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $team->image = asset('public/assets/images/teams/'.$fileName);
        }

        $team->update($request->all());

        return redirect(route('teams.index'))->with('success','Team Updated successfully!');

    }

    public function delete($id){
        $team = Team::find($id);
        $team->delete();

        return redirect(route('teams.index'))->with('success','Team Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $teams = Team::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($teams);
        if($count > 0)
            return view('teams.index', compact('teams','q'));
        else 
            return redirect(route('teams.index'))->with('error','No Records Found!');  
    }

}

