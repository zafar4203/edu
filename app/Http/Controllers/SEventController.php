<?php

namespace App\Http\Controllers;

use Validator;
use App\Tag;
use App\ChildTag;
use App\Event;
use App\Category;
use Illuminate\Http\Request;

class SEventController extends Controller
{
    public function index(){
        $events = Event::paginate(10);
        return view('events.index', compact('events'));
    }

    public function add(){
        $categories = Category::where(['status' => 0 ,'type' => 'event'])->get();
        $tags = Tag::where(['status' => 0,'type' => 'event'])->get();
        return view('events.add',compact('categories','tags'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'city' => 'required|max:255',
            'state' => 'required',
            'country' => 'required',
            'title' => 'required',
            'text' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'place' => 'required',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $event = new Event();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/events';
            $fileName = $event->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/events/'.$fileName);
        }
        $request->merge([
            'image' => $img,
            'childtags_id' => json_encode($request->childtags_id)
        ]);
        $event->create($request->all());

        return redirect(route('events.index'))->with('success','Event Created successfully!');

    }

    public function edit($id){
        $event = Event::find($id);
        $tags = Tag::where(['status' => 0,'type' => 'event'])->get();
        $categories = Category::where(['status' => 0,'type' => 'event'])->get();
        return view('events.edit' , compact('tags','categories','event'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'city' => 'required|max:255',
            'state' => 'required',
            'country' => 'required',
            'title' => 'required',
            'text' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'place' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $event = Event::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/events';
            $fileName = $event->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $event->image = asset('public/assets/images/events/'.$fileName);
        }

        $event->update($request->all());

        return redirect(route('events.index'))->with('success','Event Updated successfully!');

    }

    public function delete($id){
        $event = Event::find($id);
        $event->delete();
        return redirect(route('events.index'))->with('success','Event Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $events = Event::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($events);
        if($count > 0)
            return view('events.index', compact('events','q'));
        else 
            return redirect(route('events.index'))->with('error','No Records Found!');  
    }

    public function allChildTags($id){
        $tags = ChildTag::where(['tag_id' => $id])
        ->with('allChildTags')
        ->get();

        return response()->json(array('success' => true , 'tags' =>$tags), 200);
    }

}

