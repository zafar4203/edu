<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Tag;
use App\ChildTag;
use App\Mcq;
use App\Category;
use Illuminate\Http\Request;

class SMcqController extends Controller
{
    public function index(){
        $mcqs = Mcq::paginate(10);
        return view('mcqs.index', compact('mcqs'));
    }

    public function add(){
        $categories = Category::where(['status' => 0])->get();
        $tags = Tag::where(['status' => 0])->get();
        return view('mcqs.add',compact('categories','tags'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'question' => 'required|max:255',
            'options' => 'required',
            'c_answer' => 'required',
            'city' => 'required',
            'country' => 'required',
            'state' => 'required',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $img = "";
        $mcq = new Mcq();
        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/mcqs';
            $fileName = $mcq->id . rand() . ".png";
            $success = $file->move($path, $fileName);
            $img = asset('public/assets/images/mcqs/'.$fileName);
        }
        $request->merge([
            'image' => $img,
            'user_id' => Auth::user()->id,
            'childtags_id' => json_encode($request->childtags_id)
        ]);
        $mcq->create($request->all());

        return redirect(route('mcqs.index'))->with('success','Mcq Created successfully!');

    }

    public function edit($id){
        $mcq = Mcq::find($id);
        $tags = Tag::where(['status' => 0])->get();
        $categories = Category::where(['status' => 0])->get();
        return view('mcqs.edit' , compact('tags','categories','mcq'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'seo_title' => 'nullable|max:255',
            'seo_description' => 'nullable|max:255',
            'category_id' => 'required',
            'tag_id' => 'required',
            'question' => 'required|max:255',
            'options' => 'required',
            'c_answer' => 'required',
            'city' => 'required',
            'country' => 'required',
            'state' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $mcq = Mcq::find($request->id);

        if($request->file){
            $file = $request->file;
            $path = 'public/assets/images/mcqs';
            $fileName = $mcq->id . rand() . ".png";
            $success1 = $file->move($path, $fileName);
            $mcq->image = asset('public/assets/images/mcqs/'.$fileName);
        }

        $mcq->update($request->all());

        return redirect(route('mcqs.index'))->with('success','Mcq Updated successfully!');

    }

    public function delete($id){
        $mcq = Mcq::find($id);
        $mcq->delete();
        return redirect(route('mcqs.index'))->with('success','Mcq Deleted successfully!');
    }

    public function search(Request $request){
        $q = $request->search;
        $mcqs = Mcq::where('name','LIKE','%'.$q.'%')->paginate(20);
        $count = count($mcqs);
        if($count > 0)
            return view('mcqs.index', compact('mcqs','q'));
        else 
            return redirect(route('mcqs.index'))->with('error','No Records Found!');  
    }

    public function allChildTags($id){
        $tags = ChildTag::where(['tag_id' => $id])
        ->with('allChildTags')
        ->get();

        return response()->json(array('success' => true , 'tags' =>$tags), 200);
    }

}

