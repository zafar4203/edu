<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function tag(){
        return $this->belongsTo('App\Tag','tag_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
