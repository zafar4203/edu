<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
Route::get('/', function () {
    return view('welcome');
});

// Admin Management
Route::get('admin','SAdminController@index')->name('admins.index');
Route::get('admin/add','SAdminController@add')->name('admins.add');
Route::post('admin/store','SAdminController@store')->name('admins.store');
Route::get('admin/edit/{id}','SAdminController@edit')->name('admins.edit');
Route::post('admin/update','SAdminController@update')->name('admins.update');
Route::get('admin/delete/{id}','SAdminController@delete')->name('admins.delete');
Route::get('admin/search','SAdminController@search')->name('admins.search');

// Team Management
Route::get('team','STeamController@index')->name('teams.index');
Route::get('team/add','STeamController@add')->name('teams.add');
Route::post('team/store','STeamController@store')->name('teams.store');
Route::get('team/edit/{id}','STeamController@edit')->name('teams.edit');
Route::post('team/update','STeamController@update')->name('teams.update');
Route::get('team/delete/{id}','STeamController@delete')->name('teams.delete');
Route::get('team/search','STeamController@search')->name('teams.search');

// Event Management
Route::get('event','SEventController@index')->name('events.index');
Route::get('event/add','SEventController@add')->name('events.add');
Route::post('event/store','SEventController@store')->name('events.store');
Route::get('event/edit/{id}','SEventController@edit')->name('events.edit');
Route::post('event/update','SEventController@update')->name('events.update');
Route::get('event/delete/{id}','SEventController@delete')->name('events.delete');
Route::get('event/search','SEventController@search')->name('events.search');
Route::get('event/allChildTags/{id}','SEventController@allChildTags')->name('events.allChildTags');

// Blog Management
Route::get('blog','SBlogController@index')->name('blogs.index');
Route::get('blog/add','SBlogController@add')->name('blogs.add');
Route::post('blog/store','SBlogController@store')->name('blogs.store');
Route::get('blog/edit/{id}','SBlogController@edit')->name('blogs.edit');
Route::post('blog/update','SBlogController@update')->name('blogs.update');
Route::get('blog/delete/{id}','SBlogController@delete')->name('blogs.delete');
Route::get('blog/search','SBlogController@search')->name('blogs.search');
Route::get('blog/allChildTags/{id}','SBlogController@allChildTags')->name('blogs.allChildTags');

// Category Management
Route::get('category/add','SCategoryController@add')->name('categories.add');
Route::post('category/store','SCategoryController@store')->name('categories.store');
Route::get('category/edit/{id}','SCategoryController@edit')->name('categories.edit');
Route::post('category/update','SCategoryController@update')->name('categories.update');
Route::get('category/delete/{id}','SCategoryController@delete')->name('categories.delete');
Route::get('category/search','SCategoryController@search')->name('categories.search');
Route::get('category/{type?}','SCategoryController@index')->name('categories.index');

// Tag Management
Route::get('tag/add','STagController@add')->name('tags.add');
Route::post('tag/store','STagController@store')->name('tags.store');
Route::get('tag/edit/{id}','STagController@edit')->name('tags.edit');
Route::post('tag/update','STagController@update')->name('tags.update');
Route::get('tag/delete/{id}','STagController@delete')->name('tags.delete');
Route::get('tag/search','STagController@search')->name('tags.search');
Route::get('tag/{type?}','STagController@index')->name('tags.index');

//Child Tags Management
Route::get('childtag/add','SChildTagController@add')->name('childtags.add');
Route::post('childtag/store','SChildTagController@store')->name('childtags.store');
Route::get('childtag/edit/{id}','SChildTagController@edit')->name('childtags.edit');
Route::post('childtag/update','SChildTagController@update')->name('childtags.update');
Route::get('childtag/delete/{id}','SChildTagController@delete')->name('childtags.delete');
Route::get('childtag/search','SChildTagController@search')->name('childtags.search');
Route::get('childtag/{type?}','SChildTagController@index')->name('childtags.index');

// MCQ Management
Route::get('mcq','SMcqController@index')->name('mcqs.index');
Route::get('mcq/add','SMcqController@add')->name('mcqs.add');
Route::post('mcq/store','SMcqController@store')->name('mcqs.store');
Route::get('mcq/edit/{id}','SMcqController@edit')->name('mcqs.edit');
Route::post('mcq/update','SMcqController@update')->name('mcqs.update');
Route::get('mcq/delete/{id}','SMcqController@delete')->name('mcqs.delete');
Route::get('mcq/search','SMcqController@search')->name('mcqs.search');
Route::get('mcq/allChildTags/{id}','SMcqController@allChildTags')->name('mcq.allChildTags');

// Role Management
Route::get('role','SRoleController@index')->name('roles.index');
Route::get('role/add','SRoleController@add')->name('roles.add');
Route::post('role/store','SRoleController@store')->name('roles.store');
Route::get('role/edit/{id}','SRoleController@edit')->name('roles.edit');
Route::post('role/update','SRoleController@update')->name('roles.update');
Route::get('role/delete/{id}','SRoleController@delete')->name('roles.delete');
Route::get('role/search','SRoleController@search')->name('roles.search');

Route::get('/home', 'HomeController@index')->name('home');
});


// Frontend
Route::get('front/mcq/categories','HomeController@categories_mcq')->name('mcq.categories');
Route::get('front/mcq/tags/{cat_id}','HomeController@categories_tags')->name('mcq.tags');
Route::get('front/mcq/test/{cat_id}/{tag_id?}/{child_tags?}','HomeController@categories_test')->name('mcq.test');


Auth::routes();
